package com.jay.pc_manager.repository;

import com.jay.pc_manager.entity.PcCustomer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PcCustomerRepository extends JpaRepository<PcCustomer,Long> {

}
