package com.jay.pc_manager.service;

import com.jay.pc_manager.entity.PcCustomer;
import com.jay.pc_manager.repository.PcCustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PcCustomerService {
    private final PcCustomerRepository pcCustomerRepository;

    public void SetPcCustomer(String pcName,String name) {
        PcCustomer addData = new PcCustomer();
        addData.setPcName(pcName);
        addData.setName(name);

        pcCustomerRepository.save(addData);
    }
}
