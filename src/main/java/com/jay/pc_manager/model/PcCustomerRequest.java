package com.jay.pc_manager.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PcCustomerRequest {
    private String pcName;
    private String name;
}
