package com.jay.pc_manager.controller;


import com.jay.pc_manager.entity.PcCustomer;
import com.jay.pc_manager.model.PcCustomerRequest;
import com.jay.pc_manager.service.PcCustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/customer")
public class PcCustomerController {
    private final PcCustomerService pcCustomerService;
    @PostMapping("/data")
    public String setCustomer(@RequestBody PcCustomerRequest request){
    pcCustomerService.SetPcCustomer(request.getPcName(), request.getName());

    return "OK";
    }
}
